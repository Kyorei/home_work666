# task 1
# Напишіть код, який зформує строку, яка містить певну інформацію про символ за його номером у слові.
# Наприклад "The [номер символу] symbol in '[тут слово]' is '[символ з відповідним порядковим номером в слові]'".
# Слово та номер символу отримайте за допомогою input() або скористайтеся константою.
# Наприклад (слово - "Python" а номер символу 3) - "The 3 symbol in 'Python' is 't' ".

word = (input('Write your combination to analyze :'))
num = int(input('Enter NUMBER of the symbol :' ))

if int(num) >= 0 :
    print('The ' + str(num) + ' symbol in ' +  '"' + str(word) + '"'  + ' is ' +  '"' + word[int(num)]  + '"' )


# Але ChatGPT вирішив все простіше, красивіше і еффективніше... і пояснив мені:
word = input("Enter a word: ")
position = int(input("Enter the position of the symbol: "))

symbol = word[position - 1]

message = f"The {position} symbol in '{word}' is '{symbol}'."
print(message)


# 12




# task 2
# Вести з консолі строку зі слів за допомогою input() (або скористайтеся константою). Напишіть код, який визначить кількість слів, в цих даних.

phrase = input("Введіть рядок слів: ")

words = phrase.split()

word_count = len(words)

print("Кількість слів:", word_count)

# task 3
# Існує ліст з різними даними, наприклад lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
# Напишіть код, який сформує новий list (наприклад lst2), який би містив всі числові змінні (int, float), які є в lst1.
# Майте на увазі, що данні в lst1 не є статичними можуть змінюватись від запуску до запуску.

lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
lst2 = []

for item in lst1:
    if isinstance(item, (int, float)) and not isinstance(item, bool):
        lst2.append(item)

print(lst2)

#Останню задачу також вирішив на 40-50% за допомогою ЧатуГеПеТе - він підказав функцію " isinstance(item, (int, float)) ".
# Але його код видавав ще й bool, тому я доробив там умову " and not isinstance(item, bool): ", щоб видавало в результаті лише числові (int,float) значення.